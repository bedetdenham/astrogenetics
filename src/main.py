#Creates majority-rules consensus tree using 100 bootstraps
import time
import pandas as pd
import numpy as np
import subprocess as sub
from skbio import TreeNode
from skbio.tree import majority_rule
from skbio import write
import skbio as sk
import scipy.spatial as sp
import matplotlib.pyplot as plt
from io import StringIO

#Load data
df = pd.read_csv(r'.\data\firstByr.csv')
df2 = df.drop_duplicates()
s = df2

#Select label columns and matrix columns
labels = s.iloc[:,0:2]
m= s.iloc[:,2:39]

#Initialise
col_vals = np.array(range(37))
trees = []
start = time.time()

#Bootstrap matrices and create trees
for t in range(100):
    col_rands = np.random.choice(col_vals, size=37)
    mre = m.iloc[:,col_rands].to_numpy()
    D = sp.distance_matrix(mre,mre,p=2)


    file_out = "data/matrix.mldist"

    with open(file_out, 'w') as file_object:
        file_object.write(str(len(m)) + '\n')
        for i in range(len(labels)):
            t = list()
            for j in range(len(m)):
                t.append(str.format('{0:.8f}', D[i][j]))
            st = ' '    
            file_object.write(str(i) + '\t' + st.join(t) + '\n')

    out = sub.run([r'.\decenttree.exe', "-in", file_out, "-std-out", "-no-banner", "-q"], capture_output = True).stdout

    trees.append(TreeNode.read(StringIO(out.decode())))

    #Appends bootstrapped trees to file
    with open("data/bootstrapTrees.newick", 'ab') as file_object:
        file_object.write(out)

#Creat consensus tree
consensus = majority_rule(trees, cutoff = 0.5)[0]

#Write consensus tree to newick format for viewing
sk.io.write(consensus, format ='newick', into = 'data/consensus.newick')

#Initialise tree information lists
stars=[]
radii=[]
age=[]
nodes=[]
depths=[]

#Extract information lists
for node in consensus.tips():
    stars.append(node.name)
    radii.append(labels.iloc[int(node.name),1])
    age.append(labels.iloc[int(node.name),0])
    nodes.append(node)
    depths.append(len(node.ancestors()))

#Create dataframe
dfstars=pd.DataFrame()
dfstars['Index'] = stars
dfstars['Radii'] = radii
dfstars['Age'] = age
dfstars['Depth'] = depths

#Get runtime
end = time.time()
total_time = end-start
print("\n" + str(total_time))
