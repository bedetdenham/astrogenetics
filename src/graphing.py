#Graphs

#Main result
plt.scatter(dfstars["Depth"],dfstars["Age"],c=dfstars["Radii"])
cbar=plt.colorbar()
cbar.set_label('Birth Radii (kpc)', rotation=90)
plt.xlabel("Number of Ancestors")
plt.ylabel("Time of Formation (Myr)")
plt.title("Relationship of Tree Position, Birth Radii and Age")
plt.show()

#Shows expansion of the Milky Way
plt.hexbin(dfstars["Radii"],dfstars["Age"],cmap=plt.cm.Blues)
plt.xlabel("Birth Radii (kpc)")
plt.ylabel("Time of Formation (Myr)")
plt.title("Dispersion of Birth Radii Over Time")
plt.show()