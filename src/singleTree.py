#Single tree, no bootstrapping or consensus tree
import time
import pandas as pd
import numpy as np
import subprocess as sub
from skbio import TreeNode
from skbio.tree import majority_rule
import scipy.spatial as sp
import matplotlib.pyplot as plt
from io import StringIO
df = pd.read_csv(r'.\data\firstByr.csv')
df2 = df.drop_duplicates()
s = df2
labels = s.iloc[:,0:2]
m= s.iloc[:,2:39]

trees = []

start = time.time()

D = sp.distance_matrix(m,m,p=2)

file_out = "data/matrix.mldist"

with open(file_out, 'w') as file_object:
    file_object.write(str(len(m)) + '\n')
    for i in range(len(labels)):
        t = list()
        for j in range(len(m)):
            t.append(str.format('{0:.8f}', D[i][j]))
        st = ' '    
        file_object.write(str(i) + '\t' + st.join(t) + '\n')

out = sub.run([r'.\decenttree.exe', "-in", file_out, "-std-out", "-no-banner", "-q"], capture_output = True).stdout

trees.append(TreeNode.read(StringIO(out.decode())))

with open("data/tree.newick", 'wb') as file_object:
    file_object.write(out)

tree = trees[0]

stars=[]
radii=[]
age=[]
nodes=[]
depths=[]
for node in tree.tips():
    stars.append(node.name)
    radii.append(labels.iloc[int(node.name),1])
    age.append(labels.iloc[int(node.name),0])
    nodes.append(node)
    depths.append(len(node.ancestors()))

dfstars=pd.DataFrame()
dfstars['Index'] = stars
dfstars['Radii'] = radii
dfstars['Age'] = age
dfstars['Depth'] = depths

end = time.time()
total_time = end-start
print("\n" + str(total_time))
