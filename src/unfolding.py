#Unfolding

#make subtrees from starting nodes of branches. Found by manual inspection
tree1 = finaltree.find('1054').parent #top
tree2 = finaltree.find('826').parent #thin
tree3 = finaltree.find('1052').parent.parent #below thin
tree4 = finaltree.find('997').parent #mid
tree5 = finaltree.find('827').parent.parent #bot

def unfoldtree(tree):
    stars=[]
    radii=[]
    age=[]
    nodes=[]
    depths=[]
    for node in tree.tips():
        stars.append(node.name)
        radii.append(labels.iloc[int(node.name),1])
        age.append(labels.iloc[int(node.name),0])
        nodes.append(node)
        depths.append(len(node.ancestors()))

    dfstars1=pd.DataFrame()
    dfstars1['Index'] = stars
    dfstars1['Radii'] = radii
    dfstars1['Age'] = age
    dfstars1['Depth'] = depths
    dfstars1['Depthr']=dfstars1['Depth'].max()-dfstars1['Depth']

    return dfstars1

    #merge all dataframes

#make dataframes
df1 = unfoldtree(tree1)
df2 = unfoldtree(tree2)
df3 = unfoldtree(tree3)
df4 = unfoldtree(tree4)
df5 = unfoldtree(tree5)

def graphing(df):
    plt.scatter(df["Depthfinal"],df["Age"],c=df["Radii"])
    cbar=plt.colorbar()
    cbar.set_label('Birth Radii (kpc)', rotation=90)
    plt.xlabel("Number of Ancestors")
    plt.ylabel("Time of Formation (Myr)")
    plt.title("Relationship of Tree Position, Birth Radii and Age")
    plt.show()

#final depths with adjustments made by manual inspection of starting depth
df1['Depthfinal']=df1['Depth'] +331
df2['Depthfinal']=df2['Depth'] +286
df3['Depthfinal']=df3['Depth'] +338
df4['Depthfinal']=df4['Depthr'] +204
df5['Depthfinal']=df5['Depthr']

#combine dataframes
dfs = pd.concat([df1, df2, df3, df4, df5])